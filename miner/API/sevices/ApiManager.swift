//
//  ApiManager.swift
//  miner
//
//  Created by Maxim on 20.03.2022.
//

import Foundation

class ApiManager {
    
    static let shared = ApiManager()
    
    static var adress = ""
    
    
    func getMiner(completion: @escaping (Miner) -> Void) {
        let url = URL(string: "https://api.ethermine.org/miner/\(ApiManager.adress)/dashboard")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) {data , response, error in
            if let data = data, let miner = try? JSONDecoder().decode(Miner.self, from: data) {
                completion(miner)
            }
        }.resume()
    }
    
    func getWorkers(completion: @escaping (Workers) -> Void) {
        let url = URL(string: "https://api.ethermine.org/miner/\(ApiManager.adress)/workers")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) {data , response, error in
            if let data = data, let worker = try? JSONDecoder().decode(Workers.self, from: data) {
                completion(worker)
            }
        }.resume()
    }
    
    func getCrypto(completion: @escaping (Crypto) -> Void) {
        let url = URL(string: "https://api.ethermine.org/poolStats")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data, let crypto = try? JSONDecoder().decode(Crypto.self, from: data) {
                completion(crypto)
            }
        }.resume()
    }
    
    func getRevenue(hashrate: String, completion: @escaping (Revenue) -> Void) {
        let url = URL(string: "https://whattomine.com/coins/151.json?hr=\(hashrate)")!
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data, let revenue = try? JSONDecoder().decode(Revenue.self, from: data) {
                completion(revenue)
            }
        }.resume()
    }
}
