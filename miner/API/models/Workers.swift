//
//  Workers.swift
//  miner
//
//  Created by Maxim on 21.03.2022.
//

import Foundation

// MARK: - Workers
struct Workers: Codable {
    let status: String?
    let data: [Datum]?
}

// MARK: - Datum
struct Datum: Codable {
    let worker: String?
    let time, lastSeen, reportedHashrate, currentHashrate: Int?
    let validShares, invalidShares, staleShares: Int?
    let averageHashrate: Double?
}
