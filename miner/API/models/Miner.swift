//
//  Miner.swift
//  miner
//
//  Created by Maxim on 21.03.2022.
//

import Foundation

// MARK: - User
struct Miner: Codable {
    let status: String?
    let data: DataClass?
}

// MARK: - DataClass
struct DataClass: Codable {
    let statistics, workers: [CurrentStatistics]?
    let currentStatistics: CurrentStatistics?
    let settings: Settings?
}

// MARK: - CurrentStatistics
struct CurrentStatistics: Codable {
    let time: Date?
    let lastSeen: Int?
    let currentHashrate, reportedHashrate: Double?
    let validShares, invalidShares, staleShares, activeWorkers: Int?
    let unpaid: Int?
    let worker: String?
}

// MARK: - Settings
struct Settings: Codable {
    let email: String?
    let monitor: Int?
    let minPayout: Double?
    let suspended: Int?
}

