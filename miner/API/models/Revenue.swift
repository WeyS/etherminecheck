//
//  Revenue.swift
//  miner
//
//  Created by Maxim on 26.03.2022.
//

import Foundation

// MARK: - Revenue
struct Revenue: Codable {
    let id: Int?
    let name, tag, algorithm, blockTime: String?
    let blockReward, blockReward24, blockReward3, blockReward7: Double?
    let lastBlock: Int?
    let difficulty, difficulty24, difficulty3, difficulty7: Double?
    let nethash: Int?
    let exchangeRate, exchangeRate24, exchangeRate3, exchangeRate7: Double?
    let exchangeRateVol: Double?
    let exchangeRateCurr, marketCap, poolFee, estimated_rewards: String?
    let btc_revenue, revenue, cost, profit: String?
    let status: String?
    let lagging, testing, listed: Bool?
    let timestamp: Int?
}
