
import Foundation

// MARK: - Crypto
struct Crypto: Codable {
    let status: String?
    let data: DataClasss?
}

// MARK: - DataClass
struct DataClasss: Codable {
    let price: Price?
}

// MARK: - Price
struct Price: Codable {
    let time: String?
    let usd, btc, eur, cny: Double?
    let rub: Int?
}
