//
//  FirstTableViewCell.swift
//  miner
//
//  Created by Maxim on 21.03.2022.
//

import UIKit

class FirstTableViewCell: UITableViewCell {

    @IBOutlet weak var activeWorkers: UILabel!
    @IBOutlet weak var inActiveWorkers: UILabel!
    @IBOutlet weak var unpaidBalance: UILabel!
    @IBOutlet weak var balanceInUsd: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(){
        
        var ethPrice: Double?
        DispatchQueue.global().sync {
            ApiManager.shared.getCrypto { crypto in
                ethPrice = crypto.data?.price?.usd
            }
    
            ApiManager.shared.getMiner { miner in
                DispatchQueue.main.async {
            
                    self.activeWorkers.text = String((miner.data?.currentStatistics?.activeWorkers) ?? 0) + " /"
            
                    let inActive = (miner.data?.workers?.count ?? 0) - (miner.data?.currentStatistics?.activeWorkers ?? 0)
                    self.inActiveWorkers.text = String(inActive)
            
                    let etherData = Double((miner.data?.currentStatistics?.unpaid)!)
                    let cacl : Double = Double(etherData / 1000000000000000000)
                    self.unpaidBalance.text = String(format: "%.5f", cacl) + "ETH"
            
                    if let ethPrice1 = ethPrice{
                        self.balanceInUsd.text = String(format: "%.2f", cacl * ethPrice1) + "USD"
                    }
                }
            }
        }
    }
    
}
