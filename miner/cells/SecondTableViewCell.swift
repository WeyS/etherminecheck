//
//  SecondTableViewCell.swift
//  miner
//
//  Created by Maxim on 23.03.2022.
//

import UIKit

class SecondTableViewCell: UITableViewCell {

    @IBOutlet weak var oneHour: UIButton!
    @IBOutlet weak var oneMonth: UIButton!
    @IBOutlet weak var oneWeek: UIButton!
    @IBOutlet weak var oneDay: UIButton!
    @IBOutlet weak var revenueDollar: UILabel!
    @IBOutlet weak var revenueBitcoin: UILabel!
    @IBOutlet weak var revenueEthereum: UILabel!
    
    var btn1 : (() -> Void)? = nil
    var btn2 : (() -> Void)? = nil
    var btn3 : (() -> Void)? = nil
    var btn4 : (() -> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func configure(){
        var hasrate: Double?
        
        ApiManager.shared.getMiner { miner in
            hasrate = (miner.data?.currentStatistics?.currentHashrate)! / 1000000
        }
        
        btn1 = {
            ApiManager.shared.getRevenue(hashrate: String(hasrate ?? 0)) { revenue in
                DispatchQueue.main.async {
                    let toDoubleUsd = Double(revenue.revenue!.dropFirst())
                    let toDoubleBitcoin = Double(revenue.btc_revenue!)
                    let toDoubleEth = Double(revenue.estimated_rewards!)
                    let oneHourUsd = (toDoubleUsd ?? 0) / (24.0)
                    let oneHourBitcoin = (toDoubleBitcoin ?? 0) / (24.0)
                    let oneHourEth = (toDoubleEth ?? 0) / (24.0)
                    self.revenueDollar.text = "$" + String(format: "%.2f", oneHourUsd)
                    self.revenueBitcoin.text = String(format: "%.8f", oneHourBitcoin)
                    self.revenueEthereum.text = String(format: "%.8f", oneHourEth)
                }
            }
        }
        
        btn2 = {
            ApiManager.shared.getRevenue(hashrate: String(hasrate ?? 0)) { revenue in
                DispatchQueue.main.async {
                    self.revenueDollar.text = String(revenue.revenue!)
                    self.revenueBitcoin.text = String(revenue.btc_revenue!)
                    self.revenueEthereum.text = String(revenue.estimated_rewards!)
                }
            }
        }
        
        btn3 = {
            ApiManager.shared.getRevenue(hashrate: String(hasrate ?? 0)) { revenue in
                DispatchQueue.main.async {
                    let toDoubleUsd = Double(revenue.revenue!.dropFirst())
                    let toDoubleBitcoin = Double(revenue.btc_revenue!)
                    let toDoubleEth = Double(revenue.estimated_rewards!)
                    let oneWeekUsd = (toDoubleUsd ?? 0) * (7.0)
                    let oneWeekBitcoin = (toDoubleBitcoin ?? 0) * (7.0)
                    let oneWeekEth = (toDoubleEth ?? 0) * (7.0)
                    self.revenueDollar.text = "$" + String(format: "%.2f", oneWeekUsd)
                    self.revenueBitcoin.text = String(format: "%.8f", oneWeekBitcoin)
                    self.revenueEthereum.text = String(format: "%.8f", oneWeekEth)
                }
            }
        }
        btn4 = {
            ApiManager.shared.getRevenue(hashrate: String(hasrate ?? 0)) { revenue in
                DispatchQueue.main.async {
                    let toDoubleUsd = Double(revenue.revenue!.dropFirst())
                    let toDoubleBitcoin = Double(revenue.btc_revenue!)
                    let toDoubleEth = Double(revenue.estimated_rewards!)
                    let oneMonthUsd = (toDoubleUsd ?? 0) * (30.0)
                    let oneMonthBitcoin = (toDoubleBitcoin ?? 0) * (30.0)
                    let oneMonthEth = (toDoubleEth ?? 0) * (30.0)
                    self.revenueDollar.text = "$" + String(format: "%.2f", oneMonthUsd)
                    self.revenueBitcoin.text = String(format: "%.8f", oneMonthBitcoin)
                    self.revenueEthereum.text = String(format: "%.8f", oneMonthEth)
                }
            }
        }
    }
    
    
    
    @IBAction func oneHourAction(_ sender: UIButton) {
        if let btnAction = self.btn1{
           btnAction()
        }
        sender.isSelected = true
        oneDay.isSelected = false
        oneMonth.isSelected = false
        oneWeek.isSelected = false
    }
    
    @IBAction func oneDayAction(_ sender: UIButton) {
        if let btnAction = self.btn2{
           btnAction()
        }
        sender.isSelected = true
        oneHour.isSelected = false
        oneWeek.isSelected = false
        oneMonth.isSelected = false
    }
    @IBAction func oneWeekAction(_ sender: UIButton) {
        if let btnAction = self.btn3{
           btnAction()
        }
        sender.isSelected = true
        oneDay.isSelected = false
        oneHour.isSelected = false
        oneMonth.isSelected = false
    }
    @IBAction func oneMonthAction(_ sender: UIButton) {
        if let btnAction = self.btn4{
           btnAction()
        }
        sender.isSelected = true
        oneDay.isSelected = false
        oneHour.isSelected = false
        oneWeek.isSelected = false
    }
    
}
