//
//  ThirdTableViewCell.swift
//  miner
//
//  Created by Maxim on 27.03.2022.
//

import UIKit
import SwiftChart

class ThirdTableViewCell: UITableViewCell, ChartDelegate {
    
    @IBOutlet weak var currentHashrate: UILabel!
    @IBOutlet weak var averageHashrate: UILabel!
    @IBOutlet weak var reportedHashrate: UILabel!
    
    @IBOutlet weak var validShares: UILabel!
    @IBOutlet weak var staleShares: UILabel!
    @IBOutlet weak var invalidShares: UILabel!
    
    @IBOutlet weak var chart: Chart!
    @IBOutlet weak var time: UILabel!
    
    var currentHS: [Double] = []
    var averageHS: [Double] = []
    var reportedHS: [Double] = []
    var date: [Double] = []
    let dateFormatter = { () -> DateFormatter in
        let dat = DateFormatter()
        dat.dateFormat = "MMM d, h:mm a"
        return dat
    }
    let dateFormatter1 = { () -> DateFormatter in
        let dat = DateFormatter()
        dat.dateFormat = "ddMMHHmm"
        return dat
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        chart.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(){
        
        ApiManager.shared.getMiner { miner in
            DispatchQueue.main.async {
                self.currentHashrate.text = String(format: "%.1f", (miner.data?.currentStatistics?.currentHashrate)! / 1000000) + " MH/s"
                
                self.reportedHashrate.text = String(format: "%.1f", (miner.data?.currentStatistics?.reportedHashrate)! / 1000000) + " MH/s"
                
                let average = ((miner.data?.currentStatistics?.currentHashrate)! + (miner.data?.currentStatistics?.reportedHashrate)!) / 2
                
                self.averageHashrate.text = String(format: "%.1f", average / 1000000) + " MH/s"
                
                self.validShares.text = String((miner.data?.currentStatistics?.validShares)!)
                self.staleShares.text = String((miner.data?.currentStatistics?.staleShares)!)
                self.invalidShares.text = String((miner.data?.currentStatistics?.invalidShares)!)
                let time = self.dateFormatter().string(from: (miner.data?.currentStatistics?.time)!)
                self.time.text = time
                
                let statsCount = miner.data?.statistics?.count ?? 0
                DispatchQueue.global().sync {
                    self.date.removeAll()
                    self.currentHS.removeAll()
                    self.averageHS.removeAll()
                    self.reportedHS.removeAll()
                    for x in 0..<statsCount{
                        let dat = miner.data?.statistics![x].time
                        let curr = String(format: "%.1f", (miner.data?.statistics![x].currentHashrate)! / 1000000)
                        let rep = String(format: "%.1f", (miner.data?.statistics![x].reportedHashrate)! / 1000000)
                        let avg = String(format: "%.1f", ((miner.data?.statistics![x].currentHashrate)! + (miner.data?.statistics![x].reportedHashrate)!) / 2 / 1000000)
                        let convert = Double(self.dateFormatter1().string(from: dat!))
                        self.date.append(convert!)
                        self.currentHS.append(Double(curr)!)
                        self.averageHS.append(Double(avg)!)
                        self.reportedHS.append(Double(rep)!)
                    }
                }
            }
        }
        
        self.chart.lineWidth = 1
        self.chart.showXLabelsAndGrid = false
        self.chart.showYLabelsAndGrid = false
        self.chart.labelFont = UIFont.systemFont(ofSize: 8)
        self.chart.topInset = 30
        let series1 = ChartSeries(self.currentHS)
        series1.color = ChartColors.yellowColor()

        let series2 = ChartSeries(self.averageHS)
        series2.color = ChartColors.redColor()

        let series3 = ChartSeries(self.reportedHS)
        series3.color = ChartColors.purpleColor()

        self.chart.add([series1, series2, series3])
        
    }
        
}

extension ThirdTableViewCell {
    func didTouchChart(_ chart: Chart, indexes: [Int?], x: Double, left: CGFloat) {
        for (seriesIndex, dataIndex) in indexes.enumerated() {
            if let value = chart.valueForSeries(seriesIndex, atIndex: dataIndex) {
                switch seriesIndex{
                case 3:
                    currentHashrate.text = "\(value)" + " MH/s"
                case 4:
                    averageHashrate.text = "\(value)" + " MH/s"
                case 5:
                    reportedHashrate.text = "\(value)" + " MH/s"
                default:
                    ""
                }
                
            }
        }
    }
    
    func didFinishTouchingChart(_ chart: Chart) {
        
    }
    
    func didEndTouchingChart(_ chart: Chart) {
        
    }
}
