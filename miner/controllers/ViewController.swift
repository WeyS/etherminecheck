//
//  ViewController.swift
//  miner
//
//  Created by Maxim on 20.03.2022.
//

import UIKit
import AudioToolbox

class ViewController: UIViewController {

    @IBOutlet weak var minerAdressTextField: UITextField!
    @IBOutlet weak var label: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.minerAdressTextField.delegate = self
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.count == 0{
            AudioServicesPlayAlertSoundWithCompletion(SystemSoundID(kSystemSoundID_Vibrate)) { }
            label.isHidden = false
            label.text = "you didn't enter anything"
            label.textColor = .red
        }else{
            ApiManager.adress = textField.text!
            ApiManager.shared.getMiner{miner in
                DispatchQueue.main.async {
                    if miner.status == "ERROR"{
                        self.label.text = "this address does not exist"
                        self.label.isHidden = false
                    }
                    else if miner.status == "OK" {
                        UserDefaults.standard.set(textField.text!, forKey: "adress")

                        let secondController = self.storyboard?.instantiateViewController(withIdentifier: "secondVC")
                        secondController!.modalPresentationStyle = .fullScreen
                        self.present(secondController!, animated: true, completion: nil)
                    }
                }
            }
        }
        return true
    }
}

