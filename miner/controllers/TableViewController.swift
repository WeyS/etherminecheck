//
//  TableViewController.swift
//  miner
//
//  Created by Maxim on 21.03.2022.
//

import UIKit
import SwiftChart

class TableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private let refreshControl = UIRefreshControl()
    var adressTableView = ""
    let dateFormatter1 = DateFormatter()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        registerCell2()
        registerCell3()
        registerCell4()
        
        
        let adress = UserDefaults.standard.string(forKey: "adress")
        if adress != nil{
            ApiManager.adress = adress!
            adressTableView = adress!
        }
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.refreshControl = refreshControl
        refreshControl.tintColor = .black
        refreshControl.attributedTitle = NSAttributedString(string: "Reload data...")
        refreshControl.addTarget(self, action: #selector(redresh), for: .allEvents)
        
    }
    
    @IBAction func leftBarButton(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: "adress")
        ApiManager.adress = ""
        tableView.reloadData()
    }
    
    @objc func redresh(){
        self.tableView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    private func registerCell() {
        let cell = UINib(nibName: "FirstTableViewCell",
                            bundle: nil)
        self.tableView.register(cell,
                                forCellReuseIdentifier: "1")
    }
    
    private func registerCell2() {
        let cell = UINib(nibName: "SecondTableViewCell",
                            bundle: nil)
        self.tableView.register(cell,
                                forCellReuseIdentifier: "2")
    }
    
    private func registerCell3() {
        let cell = UINib(nibName: "ThirdTableViewCell",
                            bundle: nil)
        self.tableView.register(cell,
                                forCellReuseIdentifier: "3")
    }
    private func registerCell4() {
        let cell = UINib(nibName: "fourthTableViewCell",
                            bundle: nil)
        self.tableView.register(cell,
                                forCellReuseIdentifier: "4")
    }

    
}
    
extension TableViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(adressTableView)"
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.textLabel?.textColor = .black
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if ApiManager.adress.isEmpty{
            let back = self.storyboard?.instantiateViewController(withIdentifier: "firstVC")
            back!.modalPresentationStyle = .fullScreen
            self.present(back!, animated: true, completion: nil)
            return 0
        }else{
            return 4
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "1") as? FirstTableViewCell
            cell?.configure()
            return cell!
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "2") as? SecondTableViewCell
            
            cell?.configure()
            
            return cell!
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "3") as? ThirdTableViewCell
            
            cell?.configure()
            
            return cell!
        }

        return UITableViewCell()
    }

}
